require 'APPconstants'
require "WEBSTAT.WEBSTATstart"

function main(Data)
   if (Data== "INIT") then
      WEBSTATstartServer(component.fields())
      return
   end
   -- Log the data so we can import samples
   iguana.logMessage(Data)
   -- Parse the request
   local R = net.http.parseRequest{data=Data}
   trace(R.params)
   -- Generate HTML response into H variable
   local H = WEB_HEADER
   
   H = H.."Path: "..R.location..WEB_VARIABLE_HEADER
   for K,V in pairs(R.params) do
      H = H.."<li>"..K.." = "..V.."</li>\n"
   end
   
   H = H..APP_FOOTER
   trace(H)
   -- Set content type as HTML
   local Headers = {}
   Headers["content-type"] = "text/html"
   -- Prevent XSS attacks - https://cheatsheetseries.owasp.org/cheatsheets/Content_Security_Policy_Cheat_Sheet.html
   Headers["Content-Security-Policy"] = "default-src 'none'; script-src 'self'; connect-src 'self'; img-src 'self'; style-src 'self'; frame-ancestors 'self'; form-action 'self';"
   -- Send the response back - done!
   net.http.respond{body=H, headers=Headers}
end
